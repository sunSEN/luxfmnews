package com.slobodchukova.cherrynews.loader;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.content.CursorLoader;
import android.util.Log;

import com.slobodchukova.cherrynews.api.LuxFMRetrofit;
import com.slobodchukova.cherrynews.database.Item;
import com.slobodchukova.cherrynews.database.News;
import com.slobodchukova.cherrynews.database.Rss;
import com.slobodchukova.cherrynews.provider.LuxFMContentProvider;

import java.io.IOException;
import java.util.ArrayList;

/**
 * @author Slobodchukova.om@gmail.com
 */

public class NewsLoader
        extends CursorLoader {

    private LoaderState loaderState;

    public NewsLoader(Context context, LoaderState loaderState, Uri uri) {
        super(context, uri, null, null, null, null);
        this.loaderState = loaderState;
    }

    @Override
    public Cursor loadInBackground() {
        if (loaderState.isLoadFromServer()) {
            try {
                Rss rss = getRssFromServer();
                ArrayList<News> newses = parseNewses(rss);
                saveIntoDb(newses);
                loaderState.setLoadFromServer(false);
            } catch (Exception e) {
                e.printStackTrace();
                loaderState.setError(e);
            }
        }
        return super.loadInBackground();
    }

    private Rss getRssFromServer() throws IOException {
        Log.d(NewsLoader.class.getSimpleName(), "=>getRssFromServer");
        LuxFMRetrofit luxFMRetrofit = new LuxFMRetrofit();
        Rss rss = luxFMRetrofit.getChannel();
        Log.d(NewsLoader.class.getSimpleName(), "=>getRssFromServer: rss=" + rss);
        return rss;
    }

    private ArrayList<News> parseNewses(Rss rss) {
        Log.d(NewsLoader.class.getSimpleName(), "=>parseNewses");
        ArrayList<News> newses = new ArrayList<>();
        for (Item item : rss.getChannel().getItems()) {
            News news = new News();
            news.setTitle(item.getTitle());
            int position = item.getDescription().indexOf(">") + 1;
            String img = item.getDescription().substring(0, position);
            String description = item.getDescription().substring(position);
            Log.d(NewsLoader.class.getSimpleName(), " => IMG: " + img);
            Log.d(NewsLoader.class.getSimpleName(), " => DESCRIPTION:" + description);
            int imageUrlStart = img.indexOf("src='")+5;
            int imageUrlEnd = img.indexOf("'", imageUrlStart);
            String imageUrl = img.substring(imageUrlStart,imageUrlEnd);
            Log.d(NewsLoader.class.getSimpleName(), " => URL:" + imageUrl);
            news.setImage(imageUrl);
            news.setDescription(description);
            news.setLink(item.getLink());
            news.setDatePub(item.getPubDate());
            newses.add(news);
        }
        Log.d(NewsLoader.class.getSimpleName(), "=>parseNewses newses=" + newses);
        return newses;
    }

    private void saveIntoDb(ArrayList<News> newses) {
        Log.d(NewsLoader.class.getSimpleName(), "=>saveIntoDb");
        getContext().getContentResolver().delete(LuxFMContentProvider.NEWS_URI, null, null);
        ContentValues newValues = new ContentValues();
        for (News news : newses) {
            newValues.clear();
            newValues.clear();
            newValues.put(News.COLUMN_TITLE, news.getTitle());
            newValues.put(News.COLUMN_DESCRIPTION, news.getDescription());
            newValues.put(News.COLUMN_IMAGE, news.getImage());
            newValues.put(News.COLUMN_DATE_PUB, news.getDatePub());
            newValues.put(News.COLUMN_LINK, news.getLink());
            getContext().getContentResolver()
                    .insert(LuxFMContentProvider.NEWS_URI, newValues);
        }
    }

    public LoaderState getLoaderState() {
        return loaderState;
    }
}
