package com.slobodchukova.cherrynews.loader;

/**
 * @author Slobodchukova.om@gmail.com
 */

public class LoaderState {

    private boolean loadFromServer;
    private Exception error;

    public LoaderState() {
        this.loadFromServer = true;
    }

    public boolean isLoadFromServer() {
        return loadFromServer;
    }

    public Exception getError() {
        return error;
    }

    public void setError(Exception error) {
        this.error = error;
    }

    public void setLoadFromServer(boolean loadFromServer) {
        this.loadFromServer = loadFromServer;
    }
}