package com.slobodchukova.cherrynews.widget;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

/**
 * @author Slobodchukova.om@gmail.com
 */
public class FullWidthLinearLayoutManager
        extends LinearLayoutManager {

    public FullWidthLinearLayoutManager(Context context) {
        super(context, RecyclerView.VERTICAL, false);
    }

    @Override
    public RecyclerView.LayoutParams generateDefaultLayoutParams() {
        return new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
    }
}
