package com.slobodchukova.cherrynews.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.slobodchukova.cherrynews.database.Favorite;
import com.slobodchukova.cherrynews.database.LuxFMSqliteOpenHelper;
import com.slobodchukova.cherrynews.database.News;

/**
 * @author Slobodchukova.om@gmail.com
 */

public class LuxFMContentProvider
        extends ContentProvider {

    private static final String AUTHORITY = "com.slobodchukova.cherrynews.provider";

    public static final Uri NEWS_URI = Uri.parse("content://" + AUTHORITY + "/" +
            News.TABLE_NAME);
    public static final Uri FAVORITE_NEWS_URI = Uri.parse("content://" + AUTHORITY + "/" +
            Favorite.TABLE_NAME);

    private static final int CODE_NEWS = 1;
    private static final int CODE_FAVORITE_NEWS = 2;

    private static final UriMatcher uriMatcher;

    private LuxFMSqliteOpenHelper luxFMSqliteOpenHelper;

    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(AUTHORITY, News.TABLE_NAME, CODE_NEWS);
        uriMatcher.addURI(AUTHORITY, Favorite.TABLE_NAME, CODE_FAVORITE_NEWS);
    }

    @Override
    public boolean onCreate() {
        luxFMSqliteOpenHelper = new LuxFMSqliteOpenHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        Cursor cursor = luxFMSqliteOpenHelper.getWritableDatabase().query(getTableName(uri),
                projection, selection, selectionArgs, null, null, sortOrder);
        return cursor;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        luxFMSqliteOpenHelper.getWritableDatabase().insert(getTableName(uri), null, values);
        getContext().getContentResolver().notifyChange(uri, null);
        return uri;
    }

    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        int row = luxFMSqliteOpenHelper.getWritableDatabase().delete(getTableName(uri), selection,
                selectionArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return row;
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        int row = luxFMSqliteOpenHelper.getWritableDatabase().update(getTableName(uri),
                values, selection, selectionArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return row;
    }

    private String getTableName(@NonNull Uri uri) {
        String tableName;
        switch (uriMatcher.match(uri)) {
            case CODE_NEWS:
                tableName = News.TABLE_NAME;
                break;
            case CODE_FAVORITE_NEWS:
                tableName = Favorite.TABLE_NAME;
                break;
            default:
                throw new IllegalArgumentException("Wrong URI: " + uri);
        }
        return tableName;
    }
}
