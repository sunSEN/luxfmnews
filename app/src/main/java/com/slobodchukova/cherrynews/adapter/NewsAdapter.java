package com.slobodchukova.cherrynews.adapter;

import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.slobodchukova.cherrynews.database.News;
import com.slobodchukova.cherrynews.viewHolder.DoNotHaveDataViewHolder;
import com.slobodchukova.cherrynews.viewHolder.LoadViewHolder;
import com.slobodchukova.cherrynews.viewHolder.NewsViewHolder;
import com.slobodchukova.cherrynews.viewHolder.RetryViewHolder;

/**
 * @author Slobodchukova.om@gmail.com
 */

public class NewsAdapter
        extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private NewsViewHolder.Listener newsListener;
    private RetryViewHolder.Listener retryListener;
    private ListState listState;
    private Cursor cursor;

    private static final int DO_NOT_HAVE_DATA_VIEW_TYPE = 0;
    private static final int LOAD_VIEW_TYPE = 1;
    private static final int NEWS_VIEW_TYPE = 2;
    private static final int ERROR_VIEW_TYPE = 3;

    public NewsAdapter(NewsViewHolder.Listener newsListener,
                       RetryViewHolder.Listener retryListener) {
        this.newsListener = newsListener;
        this.retryListener = retryListener;
        listState = new ListState(false, true, false);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        switch (viewType) {
            case DO_NOT_HAVE_DATA_VIEW_TYPE:
                viewHolder = new DoNotHaveDataViewHolder(viewGroup.getContext());
                break;
            case LOAD_VIEW_TYPE:
                viewHolder = new LoadViewHolder(viewGroup.getContext());
                break;
            case NEWS_VIEW_TYPE:
                viewHolder = new NewsViewHolder(viewGroup.getContext(), newsListener);
                break;
            case ERROR_VIEW_TYPE:
                viewHolder = new RetryViewHolder(viewGroup.getContext(), retryListener);
                break;
            default:
                viewHolder = null;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if (listState.hasData()) {
            cursor.moveToPosition(position);
            News news = News.valueOf(cursor);
            NewsViewHolder newsViewHolder = (NewsViewHolder) viewHolder;
            newsViewHolder.setData(news);
        }
    }

    @Override
    public int getItemCount() {
        int itemCount;
        if (listState.hasData()) {
            itemCount = cursor.getCount();
        } else {
            itemCount = 1;
        }
        return itemCount;
    }

    public void updateCursor(Cursor cursor, Exception error) {
        if (this.cursor != null) {
            this.cursor.close();
        }
        this.cursor = cursor;

        if (error != null) {
            listState = new ListState(false, false, true);
        } else if ((cursor != null) && (cursor.getCount() > 0)) {
            listState = new ListState(true, false, false);
        } else {
            listState = new ListState(false, false, false);
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        int viewType;
        if (listState.hasData()) {
            viewType = NEWS_VIEW_TYPE;
        } else if (listState.hasLoad()) {
            viewType = LOAD_VIEW_TYPE;
        } else if (listState.hasError()) {
            viewType = ERROR_VIEW_TYPE;
        } else {
            viewType = DO_NOT_HAVE_DATA_VIEW_TYPE;
        }
        return viewType;
    }
}