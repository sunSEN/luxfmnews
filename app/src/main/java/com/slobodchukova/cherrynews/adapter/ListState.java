package com.slobodchukova.cherrynews.adapter;

/**
 * @author Slobodchukova.om@gmail.com
 */

public class ListState {
    private boolean hasData;
    private boolean hasLoad;
    private boolean hasError;

    public ListState(boolean hasData, boolean hasLoad, boolean hasError) {
        this.hasData = hasData;
        this.hasLoad = hasLoad;
        this.hasError = hasError;
    }

    public boolean hasError() {
        return hasError;
    }

    public boolean hasData() {
        return hasData;
    }

    public boolean hasLoad() {
        return hasLoad;
    }

}
