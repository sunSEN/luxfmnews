package com.slobodchukova.cherrynews.app;

import android.app.Application;

/**
 * @author Slobodchukova.om@gmail.com
 */

public class ProjectApplication
        extends Application {

    private static ProjectApplication sProjectApplication;

    @Override
    public void onCreate() {
        super.onCreate();
        sProjectApplication = this;
    }

    public static ProjectApplication getInstance() {
        return sProjectApplication;
    }
}
