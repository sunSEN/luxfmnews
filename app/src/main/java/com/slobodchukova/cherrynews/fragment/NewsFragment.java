package com.slobodchukova.cherrynews.fragment;

import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.slobodchukova.cherrynews.R;
import com.slobodchukova.cherrynews.adapter.NewsAdapter;
import com.slobodchukova.cherrynews.database.News;
import com.slobodchukova.cherrynews.loader.LoaderState;
import com.slobodchukova.cherrynews.loader.NewsLoader;
import com.slobodchukova.cherrynews.viewHolder.NewsViewHolder;
import com.slobodchukova.cherrynews.viewHolder.RetryViewHolder;
import com.slobodchukova.cherrynews.widget.FullWidthLinearLayoutManager;

import static com.slobodchukova.cherrynews.provider.LuxFMContentProvider.FAVORITE_NEWS_URI;
import static com.slobodchukova.cherrynews.provider.LuxFMContentProvider.NEWS_URI;

/**
 * @author Slobodchukova.om@gmail.com
 */

public class NewsFragment
        extends BaseFragment
        implements View.OnClickListener,
        LoaderManager.LoaderCallbacks<Cursor>,
        NewsViewHolder.Listener,
        RetryViewHolder.Listener {

    private static final int NEWS_LOADER_ID = 1;
    private static final int FAVORITE_NEWS_LOADER_ID = 2;

    private RecyclerView mainRecyclerView;
    private NewsAdapter newsAdapter;

    public static NewsFragment newInstance() {
        NewsFragment fragment = new NewsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    protected int getLayoutRes() {
        return R.layout.fragment_main;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    protected void onViewCreated() {
        super.onViewCreated();

        setHasOptionsMenu(true);

        AppCompatActivity appCompatActivity = ((AppCompatActivity) getActivity());
        appCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        //FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        FloatingActionButton itemFavoriteFab = (FloatingActionButton) findViewById(R.id.itemFavoriteFab);
       /* fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             Toast.makeText(getContext(), "Booo", Toast.LENGTH_LONG);

            }
        });*/

        mainRecyclerView = (RecyclerView) findViewById(R.id.mainRecyclerView);

        newsAdapter = new NewsAdapter(this, this);
        mainRecyclerView.setAdapter(newsAdapter);
        mainRecyclerView.setLayoutManager(new FullWidthLinearLayoutManager(getActivity()));

        getLoaderManager().initLoader(NEWS_LOADER_ID, null, this);
    }

    @Override
    public void onNewsClick(News news) {
        startFragment(FullNewsFragment.newInstance(news), true);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Loader loader = null;
        switch (id) {
            case NEWS_LOADER_ID:
                loader = new NewsLoader(getActivity(), new LoaderState(), NEWS_URI);
                break;
            case FAVORITE_NEWS_LOADER_ID:
                loader = new NewsLoader(getActivity(), new LoaderState(), FAVORITE_NEWS_URI);
                break;
        }
        return loader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        NewsLoader productsLoader = (NewsLoader) loader;
        newsAdapter.updateCursor(data, productsLoader.getLoaderState().getError());
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        newsAdapter.updateCursor(null, null);
    }

    @Override
    public void onRetryClick() {
        getLoaderManager().restartLoader(NEWS_LOADER_ID, null, this);
    }

    @Override
    public void onClick(View view) {
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_like:
                startFragment(FavoriteFragment.newInstance(), true);
            case R.id.itemFavoriteFab:

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}