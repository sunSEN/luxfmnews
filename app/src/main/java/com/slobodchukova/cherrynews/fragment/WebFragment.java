package com.slobodchukova.cherrynews.fragment;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.webkit.WebView;
import android.widget.ProgressBar;

import com.slobodchukova.cherrynews.R;
import com.slobodchukova.cherrynews.adapter.NewsAdapter;
import com.slobodchukova.cherrynews.database.News;

import static android.content.ContentValues.TAG;
import static com.slobodchukova.cherrynews.R.id.webview;

/**
 * @author Slobodchukova.om@gmail.com
 */

public class WebFragment
        extends BaseFragment {

    private static final String KEY_NEWS = "news";

    private RecyclerView mainRecyclerView;
    private ProgressBar progressBar;
    private NewsAdapter newsAdapter;
    private WebView webView;
    private News news;
    private String url;
    private String link;

    public static WebFragment newInstance(News news) {
        WebFragment fragment = new WebFragment();
        Bundle args = new Bundle();
        args.putSerializable(KEY_NEWS, news);
        fragment.setArguments(args);
        return fragment;
    }

    protected int getLayoutRes() {
        return R.layout.fragment_web;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onViewCreated() {
        super.onViewCreated();
        setHasOptionsMenu(true);
        //progressBar = (ProgressBar) findViewById(R.id.progressBar);
        //progressBar.setVisibility(ProgressBar.VISIBLE);
        String url = new String();
        news = (News) getArguments().getSerializable(KEY_NEWS);
        webView = (WebView) findViewById(webview);
        url = news.getLink();
        Log.d(TAG, "getLINK:" + url);
        webView.loadUrl(url);
        /*webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.d(TAG, "updateLINK:" + url);
                WebFragment.this.url = url;
                view.loadUrl(url);
                return false;
            }
        });
        webView.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView view, String url) {
                if (TextUtils.equals(url, WebFragment.this.url)) {
                }
                progressBar.setVisibility(ProgressBar.GONE);

            }
        });
        webView.setPadding(0, 0, 0, 0);
        webView.setInitialScale(60);
        AppCompatActivity appCompatActivity = ((AppCompatActivity) getActivity());
        appCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
*/
        /////////
        //----webView.getSettings().setJavaScriptEnabled(true);
        //webView.getSettings().setLoadWithOverviewMode(true);
        //webView.getSettings().setUseWideViewPort(true);
        //-- -- webView.getSettings().setMinimumFontSize(40);

        /////////
        //WebView web = new WebView(getContext());
        //web.setPadding(0, 0, 0, 0);
        //web.setInitialScale(getScale());


       /* webView.setInitialScale((int) getResources().getDimension(R.dimen._50sdp)); // getActivity(). if you are at Fragment
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webView.loadDataWithBaseURL(null,here comes html content,"text/html","UTF-8", null);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setDisplayZoomControls(true);
        webView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);*/

    }

   /* private int getScale(){
        Display display = ((WindowManager)getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        int width = display.getWidth();
        int PIC_WIDTH= webView.getRight()-webView.getLeft();
        Double val = new Double(width)/new Double(PIC_WIDTH);
        val = val * 100d;
        return val.intValue();
    }*/



   /* @Override
    protected void onSizeChanged(int w, int h, int ow, int oh) {
        // if width is zero, this method will be called again
        if (w != 0) {
            Double scale = Double.valueOf(w) / Double.valueOf(WEB_PAGE_WIDTH);
            scale *= 100d;
            setInitialScale(scale.intValue());
        }
        super.onSizeChanged(w, h, ow, oh);
    }*/


  /*  @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_site:
                link = news.getLink();
                webView.loadUrl(link);
                webView.setWebViewClient(new WebViewClient() {
                    public boolean shouldOverrideUrlLoading(WebView view, String url) {
                        if (url != null && url.startsWith(link)) {
                            return false;
                        } else {
                            view.getContext().startActivity(
                                    new Intent(Intent.ACTION_VIEW, Uri.parse(link)));
                            return true;
                        }
                    }
                });

            default:
                return super.onOptionsItemSelected(item);
        }

    }*/
}