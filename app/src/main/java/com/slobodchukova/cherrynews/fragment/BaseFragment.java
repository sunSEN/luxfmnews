package com.slobodchukova.cherrynews.fragment;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.slobodchukova.cherrynews.R;
import com.slobodchukova.cherrynews.activity.BaseActivity;

/**
 * @author Slobodchukova.om@gmail.com
 */

public abstract class BaseFragment
        extends Fragment {

    protected View rootView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(getLayoutRes(), null, false);
        onViewCreated();

        return rootView;
    }

    protected void onViewCreated() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tbBack);
        AppCompatActivity appCompatActivity = ((AppCompatActivity) getActivity());
        appCompatActivity.setSupportActionBar(toolbar);

        appCompatActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    protected abstract
    @LayoutRes
    int getLayoutRes();

    protected void startFragment(android.support.v4.app.Fragment fragment, boolean addToBackStack) {
        ((BaseActivity) getActivity()).startFragment(fragment, addToBackStack);
    }

    protected View findViewById(@IdRes int id) {
        return rootView.findViewById(id);
    }

    protected void finish() {
        getActivity().getSupportFragmentManager().popBackStack();
    }
}