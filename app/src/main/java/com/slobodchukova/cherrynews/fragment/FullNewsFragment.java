package com.slobodchukova.cherrynews.fragment;

import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.slobodchukova.cherrynews.R;
import com.slobodchukova.cherrynews.database.Favorite;
import com.slobodchukova.cherrynews.database.News;
import com.slobodchukova.cherrynews.provider.LuxFMContentProvider;
import com.slobodchukova.cherrynews.viewHolder.RetryViewHolder;
import com.squareup.picasso.Picasso;

/**
 * @author Slobodchukova.om@gmail.com
 */

public class FullNewsFragment
        extends BaseFragment
        implements View.OnClickListener,
        RetryViewHolder.Listener {

    private static final String KEY_NEWS = "news";

    private TextView newsTitleTextView;
    private TextView newsDescriptionTextView;
    private TextView newsLinkTextView;
    private TextView newsDateTextView;
    private TextView seeOnSite;
    private ImageView newsImageView;
    private News news;
    Picasso picasso;

    public static FullNewsFragment newInstance(News news) {
        FullNewsFragment fragment = new FullNewsFragment();
        Bundle args = new Bundle();
        args.putSerializable(KEY_NEWS, news);
        fragment.setArguments(args);
        return fragment;
    }

    public FullNewsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_full_news;
    }

    @Override
    protected void onViewCreated() {
        super.onViewCreated();

        AppCompatActivity appCompatActivity = ((AppCompatActivity) getActivity());
        appCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setHasOptionsMenu(true);

        newsTitleTextView = (TextView) findViewById(R.id.itemNewsTitleTextView);
        newsDescriptionTextView = (TextView) findViewById(R.id.itemNewsDescriptionTextView);
        newsLinkTextView = (TextView) findViewById(R.id.itemNewsLinkTextView);
        newsDateTextView = (TextView) findViewById(R.id.itemNewsDateTextView);
        newsImageView = (ImageView) findViewById(R.id.itemNewsImageView);
        seeOnSite = (TextView) findViewById(R.id.seeOnSite);

        news = (News) getArguments().getSerializable(KEY_NEWS);
        newsTitleTextView.setText(news.getTitle());
        newsDescriptionTextView.setText(news.getDescription());
        newsLinkTextView.setText(news.getLink());
        newsLinkTextView.setOnClickListener(this);
        seeOnSite.setOnClickListener(this);
        newsDateTextView.setText(news.getDatePub());

        Picasso.with(getContext())
                .load(news.getImage())
                .fit().centerCrop()
                .placeholder(R.drawable.load)
                .into(newsImageView);
    }

    @Override
    public void onClick(View view) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(news.getLink()));
        switch (view.getId()) {
            case R.id.itemNewsLinkTextView:
                //startFragment(WebFragment.newInstance(news), true);
                startActivity(browserIntent);
                break;
            case R.id.seeOnSite:
                startActivity(browserIntent);
                break;
            case R.id.itemFavoriteFab:
                Toast.makeText(getContext(), "News add to falorite", Toast.LENGTH_LONG).show();
                break;
        }
    }

    @Override
    public void onRetryClick() {
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_like:
                Toast.makeText(getContext(), "Add to favorite", Toast.LENGTH_LONG).show();

                ContentValues newValues = new ContentValues();
                newValues.put(Favorite.COLUMN_TITLE, news.getTitle());
                newValues.put(Favorite.COLUMN_DESCRIPTION, news.getDescription());
                newValues.put(Favorite.COLUMN_IMAGE, news.getImage());
                newValues.put(Favorite.COLUMN_DATE_PUB, news.getDatePub());
                newValues.put(Favorite.COLUMN_LINK, news.getLink());
                getContext().getContentResolver()
                        .insert(LuxFMContentProvider.FAVORITE_NEWS_URI, newValues);
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}