package com.slobodchukova.cherrynews.activity;

import android.os.Bundle;

import com.slobodchukova.cherrynews.R;
import com.slobodchukova.cherrynews.fragment.NewsFragment;

public class MainActivity
        extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState == null) {
            startFragment(NewsFragment.newInstance(), false);
        }
    }
}