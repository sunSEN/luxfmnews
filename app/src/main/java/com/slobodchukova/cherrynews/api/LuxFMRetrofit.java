package com.slobodchukova.cherrynews.api;

import android.util.Log;

import com.slobodchukova.cherrynews.database.Rss;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;
import retrofit2.http.GET;

/**
 * @author Slobodchukova.om@gmail.com
 */

public class LuxFMRetrofit {

    private static final String NEWS_PATH = "news";

    private String BASE_URL = "http://lux.fm/";
    private LuxFMInterface service;

    public interface LuxFMInterface {
        @GET("feed/overall.xml")
        Call<Rss> getChannel();
    }

    public LuxFMRetrofit() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .baseUrl(BASE_URL)
                .client(httpClient.build())
                .build();
        service = retrofit.create(LuxFMInterface.class);
    }

    public Rss getChannel()
            throws IOException {
        Call<Rss> newsCall = service.getChannel();
        Rss rss = null;
        Response<Rss> newsResponse = newsCall.execute();
        if (newsResponse.isSuccessful()) {
            rss = newsResponse.body();
        }
        if (rss == null) {
            rss = new Rss();
        }
        Log.d(LuxFMRetrofit.class.getSimpleName(), rss + " ");
        return rss;
    }
}
