package com.slobodchukova.cherrynews.viewHolder;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.slobodchukova.cherrynews.R;
import com.slobodchukova.cherrynews.database.News;
import com.slobodchukova.cherrynews.loader.NewsLoader;
import com.squareup.picasso.Picasso;

/**
 * @author Slobodchukova.om@gmail.com
 */

public class NewsViewHolder
        extends BaseViewHolder
        implements View.OnClickListener {

    public interface Listener {
        void onNewsClick(News news);
    }

    private TextView itemNewsTitleTextView;
    private TextView newsLinkTitleTextView;
    private TextView newsDescriptionTitleTextView;
    private TextView itemNewsDateTitleTextView;
    private Listener listener;
    private ImageView itemNewsImage;
    private News news;
    private int position;
    Picasso picasso;
    private String url;


    public NewsViewHolder(Context context, Listener listener) {
        super(context, R.layout.item_news);
        this.listener = listener;
        itemView.setOnClickListener(this);
        itemNewsTitleTextView = (TextView) findViewById(R.id.itemNewsTitleTextView);
        itemNewsDateTitleTextView = (TextView) findViewById(R.id.itemNewsDateTextView);
        itemNewsImage = (ImageView) findViewById(R.id.itemNewsImageView);
    }

    @Override
    public void onClick(View v) {
        listener.onNewsClick(news);
    }

    public void setData(News news) {
        this.news = news;
        itemNewsTitleTextView.setText(news.getTitle());
        itemNewsDateTitleTextView.setText(news.getDatePub());
        Log.d(NewsLoader.class.getSimpleName(), "IMAGE URL" + news.getImage());
        String newUrl = news.getImage();
        if (!newUrl.equals(url)){
            Picasso.with(itemView.getContext())
                    .cancelTag(url);
            Log.d(NewsLoader.class.getSimpleName(), "CANCEL TAG:" + url);
        }
        url = newUrl;
        Picasso.with(itemView.getContext())
                .load(newUrl)
                .tag(newUrl)
                .placeholder(R.drawable.load)
                .fit().centerCrop()
                .into(itemNewsImage);
        Log.d(NewsLoader.class.getSimpleName(), "TAG:" + url);
    }
}
