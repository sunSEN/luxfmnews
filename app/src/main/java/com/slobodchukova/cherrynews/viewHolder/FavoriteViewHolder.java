package com.slobodchukova.cherrynews.viewHolder;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.slobodchukova.cherrynews.R;
import com.slobodchukova.cherrynews.database.News;
import com.squareup.picasso.Picasso;

/**
 * @author Slobodchukova.om@gmail.com
 */

public class FavoriteViewHolder
        extends BaseViewHolder
        implements View.OnClickListener {

    public interface Listener {
        void onFavoriteNewsClick(News news);
    }

    private TextView itemFavoriteNewsTitleTextView;
    private TextView itemFavoriteNewsDateTextView;
    private FavoriteViewHolder.Listener listener;
    private ImageView itemFavoriteNewsImage;
    private News news;
    private int position;
    Picasso picasso;
    private String url;

    public FavoriteViewHolder(Context context, Listener listener) {
        super(context, R.layout.item_favorite_news);
        this.listener = listener;
        itemView.setOnClickListener(this);
        itemFavoriteNewsTitleTextView = (TextView) findViewById(R.id.itemFavoriteNewsTitleTextView);
        itemFavoriteNewsDateTextView = (TextView) findViewById(R.id.itemFavoriteNewsDateTextView);
        itemFavoriteNewsImage = (ImageView) findViewById(R.id.itemFavoriteNewsImageView);
    }

    @Override
    public void onClick(View v) {
        listener.onFavoriteNewsClick(news);
    }
}