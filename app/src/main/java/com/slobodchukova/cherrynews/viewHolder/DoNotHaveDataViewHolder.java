package com.slobodchukova.cherrynews.viewHolder;

import android.content.Context;

import com.slobodchukova.cherrynews.R;

/**
 * @author Slobodchukova.om@gmail.com
 */

public class DoNotHaveDataViewHolder
        extends BaseViewHolder {

    public DoNotHaveDataViewHolder(Context context) {
        super(context, R.layout.item_do_not_have_data);
    }
}
