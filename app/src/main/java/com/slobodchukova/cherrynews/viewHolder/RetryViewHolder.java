package com.slobodchukova.cherrynews.viewHolder;

import android.content.Context;
import android.view.View;
import android.widget.Button;

import com.slobodchukova.cherrynews.R;

/**
 * @author Slobodchukova.om@gmail.com
 */

public class RetryViewHolder
        extends BaseViewHolder
        implements View.OnClickListener {

    public interface Listener {
        void onRetryClick();
    }

    private Listener listener;
    private Button retryButton;

    public RetryViewHolder(Context context, Listener listener) {
        super(context, R.layout.item_error);
        this.listener = listener;
        retryButton = (Button) findViewById(R.id.retryButton);
        retryButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        listener.onRetryClick();
    }
}
