package com.slobodchukova.cherrynews.viewHolder;

import android.content.Context;

import com.slobodchukova.cherrynews.R;

/**
 * @author Slobodchukova.om@gmail.com
 */

public class LoadViewHolder
        extends BaseViewHolder {

    public LoadViewHolder(Context context) {
        super(context, R.layout.item_load);
    }
}