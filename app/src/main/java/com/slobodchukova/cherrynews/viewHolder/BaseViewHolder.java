package com.slobodchukova.cherrynews.viewHolder;

import android.content.Context;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;

/**
 * @author Slobodchukova.om@gmail.com
 */

public class BaseViewHolder
        extends RecyclerView.ViewHolder {

    public BaseViewHolder(Context context, @LayoutRes int layoutRes) {
        super(LayoutInflater.from(context)
                .inflate(layoutRes, null, false));
    }

    protected View findViewById(@IdRes int id) {
        return itemView.findViewById(id);
    }
}