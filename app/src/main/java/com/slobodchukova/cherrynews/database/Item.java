package com.slobodchukova.cherrynews.database;

import org.simpleframework.xml.Element;

/**
 * @author Slobodchukova.om@gmail.com
 */

public class Item {

    @Element(name = "title", required = false)
    String title;

    @Element(name = "link", required = false)
    String link;

    @Element(name = "description", required = false)
    String description;

    @Element(name = "pubDate", required = false)
    String pubDate;

    @Element(name = "guid", required = false)
    String guid;

    @Element(name = "creator", required = false)
    String creator;

    @Element(name = "date", required = false)
    String date;

    public String getTitle() {
        return title;
    }

    public String getLink() {
        return link;
    }

    public String getDescription() {
        return description;
    }

    public String getPubDate() {
        return pubDate;
    }

    public String getGuid() {
        return guid;
    }

    public String getCreator() {
        return creator;
    }

    public String getDate() {
        return date;
    }
}
