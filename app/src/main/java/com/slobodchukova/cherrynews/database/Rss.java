package com.slobodchukova.cherrynews.database;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "rss", strict = false)
public class Rss {

    @Attribute(name = "xmlns:dc", required = false)
    String xmlns;

    @Attribute(name = "version", required = false)
    String version;

    @Element(name = "channel", required = false)
    Channel channel;

    public Channel getChannel() {
        return channel;
    }
}
