package com.slobodchukova.cherrynews.database;

import android.database.Cursor;

import java.io.Serializable;

/**
 * @author Slobodchukova.om@gmail.com
 */

public class Favorite
        implements Serializable {

    public static final String TABLE_NAME = "favorite";

    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_LINK = "link";
    public static final String COLUMN_IMAGE = "image";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_DATE_PUB = "datePub";

    public long id;
    public String title;
    public String link;
    public String image;
    public String description;
    public String datePub;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDatePub() {
        return datePub;
    }

    public void setDatePub(String datePub) {
        this.datePub = datePub;
    }

    public static News valueOf(Cursor cursor) {
        News news = new News();
        int columnIndex = cursor.getColumnIndex(News.COLUMN_ID);
        if (columnIndex != -1) {
            news.setId(cursor.getLong(columnIndex));
        }
        columnIndex = cursor.getColumnIndex(News.COLUMN_TITLE);
        if (columnIndex != -1) {
            news.setTitle(cursor.getString(columnIndex));
        }
        columnIndex = cursor.getColumnIndex(News.COLUMN_LINK);
        if (columnIndex != -1) {
            news.setLink(cursor.getString(columnIndex));
        }
        columnIndex = cursor.getColumnIndex(News.COLUMN_IMAGE);
        if (columnIndex != -1) {
            news.setImage(cursor.getString(columnIndex));
        }
        columnIndex = cursor.getColumnIndex(News.COLUMN_DESCRIPTION);
        if (columnIndex != -1) {
            news.setDescription(cursor.getString(columnIndex));
        }
        columnIndex = cursor.getColumnIndex(News.COLUMN_DATE_PUB);
        if (columnIndex != -1) {
            news.setDatePub(cursor.getString(columnIndex));
        }
        return news;
    }
}