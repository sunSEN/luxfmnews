package com.slobodchukova.cherrynews.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * @author Slobodchukova.om@gmail.com
 */

public class LuxFMSqliteOpenHelper
        extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "Perhaps.db";
    private static final int DATABASE_VERSION = 5;

    public LuxFMSqliteOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + News.TABLE_NAME + " (" +
                News.COLUMN_ID + " integer primary key , " +
                News.COLUMN_TITLE + " text, " +
                News.COLUMN_IMAGE + " text, " +
                News.COLUMN_LINK + " text, " +
                News.COLUMN_DESCRIPTION + " text, " +
                News.COLUMN_DATE_PUB + " text);");

        db.execSQL("create table " + Favorite.TABLE_NAME + " (" +
                Favorite.COLUMN_ID + " integer primary key , " +
                Favorite.COLUMN_TITLE + " text, " +
                Favorite.COLUMN_IMAGE + " text, " +
                Favorite.COLUMN_LINK + " text, " +
                Favorite.COLUMN_DESCRIPTION + " text, " +
                Favorite.COLUMN_DATE_PUB + " text);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + News.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + Favorite.TABLE_NAME);
        onCreate(db);
    }
}
