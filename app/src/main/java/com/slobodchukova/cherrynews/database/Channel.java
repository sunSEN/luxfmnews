package com.slobodchukova.cherrynews.database;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;

import java.util.ArrayList;

/**
 * @author Slobodchukova.om@gmail.com
 */

public class Channel {

    @Element(name = "title", required = false)
    String title;

    @Element(name = "link", required = false)
    String link;

    @Element(name = "description", required = false)
    String description;

    @ElementList(name = "item", required = false, inline = true)
    ArrayList<Item> items;

    public ArrayList<Item> getItems() {
        return items;
    }
}
